# Inspired by Kenneth Lange. Algorithms from THE BOOK, Chapter 2. Sorting. SIAM, 2020.


import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches


def plot_binary_search(a, b, value, m, already_excluded, left, right, title = ""):

    length_b = len(b)
    fig, ax = plt.subplots()
    bars = ax.bar(a, b, width = 0.9, linewidth = 0.7, color = "blue")
    
    my_b = []
    for l in range(length_b):
        my_b.append("x["+str(l)+"]")
        
    blue = mpatches.Patch(color = "blue", label = "not yet excluded")
    red = mpatches.Patch(color = "red", label = "x[m]")
    grey = mpatches.Patch(color = "grey", label = "already excluded")
    plt.legend(handles = [blue, red, grey], fontsize = "x-small")
    
    bars[m].set_color("red")
    for el in already_excluded:
        bars[el].set_color("grey")
    
    ax.set(xlim = (0, length_b), xticks = np.arange(length_b)+0.5, ylim = (-3, 14), yticks = np.arange(-2, 14))
    ax.set_xticklabels(my_b, minor = False)
    plt.suptitle("binary_search(x, "+str(value)+"), left = "+str(left)+", right = "+str(right), fontweight = "bold")
    plt.title(title)
    patches = ax.patches
    labels = ["" for k in range(length_b)]
    labels[m] = "x[m]"
    for patch, label in zip(patches, labels):
        x_value = patch.get_x() + patch.get_width() / 2
        y_value = patch.get_height()
        position = "bottom"
        if y_value < 0:
            position = "top"
        ax.text(x_value, y_value, label, ha = "center", va = position)
    plt.show()
   



def binary_search(x, value, already_excluded = []):        # returns index of the value in x, returns -1 if the value is missing
    a = 0
    b = len(x)-1 
    c = 0.5 + np.arange(len(x))
    while a <= b:
        m = math.floor((a + b)/2)        
        plot_binary_search(c, x, value, m, already_excluded, a, b)
        if x[m] > value:
            already_excluded.extend(range(m,b+1))
            plot_binary_search(c, x, value, m, already_excluded, a, b, "x[m] = "+str(x[m])+" > "+str(value)+" = value: exclude elements with positions from m = "+str(m)+" to b = "+str(b))
            b = m - 1
        elif x[m] < value:
            already_excluded.extend(range(a,m+1))
            plot_binary_search(c, x, value, m, already_excluded, a, b, "x[m] = "+str(x[m])+" < "+str(value)+" = value: exclude elements with positions from a = "+str(a)+" to m = "+str(m))
            a = m + 1
        else:
            plot_binary_search(c, x, value, m, already_excluded, a, b, "we found the value "+str(value)+" at position "+str(m))
            return m
    plot_binary_search(c, x, value, m, already_excluded, a, b, "left = "+str(a)+" > "+str(b)+" = right: we did not find the value "+str(value)+" in the list")
    return -1




if __name__ == "__main__":

    x = [-2, -1, 1, 2, 4, 6, 7, 9]
    print("binary_search([-2, -1, 1, 2, 4, 6, 7, 9], 3): ", (binary_search(x, 3)))
    #print("binary_search([-2, -1, 1, 2, 4, 6, 7, 9], 7): ", binary_search(x, 7))