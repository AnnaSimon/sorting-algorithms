# Inspired by Kenneth Lange. Algorithms from THE BOOK, Chapter 2. Sorting. SIAM, 2020.


import math



def binary_search(x, value):        # returns index of the value in x, returns -1 if the value is missing
	a = 0
	b = len(x)-1
	while a <= b:
		m = math.floor((a + b)/2)
		if x[m] > value:
			b = m - 1
		elif x[m] < value:
			a = m + 1
		else:
			return m
	return -1




if __name__ == "__main__":
    x = ['a', 'b', 'd', 'f', 'g'];
    print("binary_search(['a', 'b', 'd', 'f', 'g'], 'f'): ", binary_search(x, 'f'))
    x = [1, 2, 4, 7, 9];
    print("binary_search([1, 2, 4, 7, 9], 3): ", binary_search(x, 3))
    print("binary_search([1, 2, 4, 7, 9], 1): ", binary_search(x, 1))