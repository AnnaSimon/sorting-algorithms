# Inspired by Kenneth Lange. Algorithms from THE BOOK, Chapter 2. Sorting. SIAM, 2020.


import numpy as np


def quicksort(x, left=0, right = None):
	if right is None:			            # default
		right = len(x)-1
	i = np.random.randint(left, right+1)	# select a random splitting value
	pivot = x[i]				            # random pivot
	x[left], x[i] = pivot, x[left]		    # swap pivot and element on the left-hand side of the list
	i = left
	for j in range (left+1, right+1):	    # right+1 is not included, only right
		if x[j] <= pivot:
			i += 1
			x[i], x[j] = x[j], x[i]
	x[left], x[i] = x[i], pivot
	if i>left+1:
		quicksort(x, left, i-1)
	if i+1<right:
		quicksort(x, i+1, right)
	
	
	
if __name__ == "__main__":	
    x = [5, 4, 3, 1, 2, 8, 7, 6, -1]
    quicksort(x)
    print(x)
    y = ['a', 'c', 'd', 'b', 'f', 'e', 'h', 'g', 'y']
    quicksort(y)
    print(y)