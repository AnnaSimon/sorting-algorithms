# Inspired by Kenneth Lange. Algorithms from THE BOOK, Chapter 2. Sorting. SIAM, 2020.

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches



def plot_quicksort(a, b, pos_pivot, all_pivots, already_sorted, left, right, title = "", show_pivot = True):

    length_b = len(b)
    fig, ax = plt.subplots()
    bars = ax.bar(a, b, width = 0.9, linewidth = 0.7, color = "blue")
    
    my_b = []
    for l in range(length_b):
        my_b.append("x["+str(l)+"]")
        
    blue = mpatches.Patch(color = "blue", label = "not sorted")
    green = mpatches.Patch(color = "green", label = "former pivot")
    lightgreen = mpatches.Patch(color = "lightgreen", label = "correctly sorted (without being a former pivot)")
    red = mpatches.Patch(color = "red", label = "pivot")
    plt.legend(handles = [blue, green, lightgreen, red], fontsize = "x-small")

    for el in all_pivots:
        bars[el].set_color("green")
    for el in already_sorted:
            bars[el].set_color("lightgreen")
    if show_pivot == True:
        bars[pos_pivot].set_color("red")
    
    ax.set(xlim = (0, length_b), xticks = np.arange(length_b)+0.5, ylim = (-3, 11), yticks = np.arange(-2, 11))
    ax.set_xticklabels(my_b, minor = False)
    if show_pivot == True:
        plt.suptitle("quicksort(x,"+str(left)+","+str(right)+")", fontweight = "bold")
    plt.title(title)
    patches = ax.patches
    labels = ["" for k in range(length_b)]
    if show_pivot == True:
        labels[pos_pivot] = "pivot"
    for patch, label in zip(patches, labels):
        x_value = patch.get_x() + patch.get_width() / 2
        y_value = patch.get_height()
        position = "bottom"
        if y_value < 0:
            position = "top"
        ax.text(x_value, y_value, label, ha = "center", va = position)
    plt.show()
   




def quicksort(x, left=0, right = None, all_pivots = [], already_sorted = []):

    if right is None:			# default
        right = len(x)-1
        
    i = np.random.randint(left, right+1)	# select a random splitting value
    pivot = x[i]				# random pivot
    a = 0.5 + np.arange(len(x))
    plot_quicksort(a, x, i, all_pivots, already_sorted, left, right)
    x[left], x[i] = pivot, x[left]		# swap pivot and element on the left-hand side of the list
    i = left
    plot_quicksort(a, x, i, all_pivots, already_sorted, left, right, "swap pivot and element on the left-hand side of the (sub-)list")
    for j in range (left+1, right+1):	# right+1 is not included, only right
        if x[j] <= pivot:
            i += 1
            x[i], x[j] = x[j], x[i]
    x[left], x[i] = x[i], pivot
    plot_quicksort(a, x, i, all_pivots, already_sorted, left, right, "pivot sorted correctly")
    all_pivots.append(i)

    if i>left+1:
        quicksort(x, left, i-1)
    elif i == left+1:
        already_sorted.append(left)
        plot_quicksort(a, x, i, all_pivots, already_sorted, left, i-1, "left-sublist contains one element", False)
    if i+1<right:
        quicksort(x, i+1, right)
    elif i+1 == right:
        already_sorted.append(right)
        plot_quicksort(a, x, i, all_pivots, already_sorted, i+1, right, "right-sublist contains one element", False)
       
	
    
    
if __name__ == "__main__":
    #x = [5, 4, 3, 1, 2, 8, 7, 6, -1]
    x = [5, 1, 2, 7, -1]
    print("before quicksort: ", x)
    quicksort(x)
    print("after quicksort: ", x)

