# Inspired by Kenneth Lange. Algorithms from THE BOOK, Chapter 2. Sorting. SIAM, 2020.


using DataStructures


pq = PriorityQueue() 													# empty queue


pq['a'] = 10 															# enqueue 
pq['b'] = 5
pq['c'] = 15
pq['d'] = 12
pq['e'] = 20
println("Priority Queue:   ", pq)


println("Exposing lowest priority item:   ", peek(pq))					# peek


pq['d'] = 0 															# change priority
println("Priority Queue after change:   ", pq)


enqueue!(pq, 'f', 11)													# enqueue
println("Priority Queue after new insertion:   ", pq)


println("Removing lowest priority item:   ", dequeue!(pq))				# dequeue
println("Priority Queue after removal:   ", pq)


println("Priority Queue after deletion of 'c':   ", delete!(pq, 'c'))	# delete