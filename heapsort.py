# Inspired by Kenneth Lange. Algorithms from THE BOOK, Chapter 2. Sorting. SIAM, 2020.


import math


def heapsort(x):
    n = len(x)
    for parent in range(math.floor(n/2),0,-1): # form the heap
        siftdown(x, parent, n)
    for bottom in range(n,1,-1): # sort the heap
        x[0], x[bottom-1] = x[bottom-1], x[0]
        siftdown(x, 1, bottom - 1)


def siftdown(x, parent, bottom):
    parent_value = x[parent-1]
    child = 2 * parent
    while child <= bottom:
        if child < bottom and x[child-1] < x[child]:
            child = child + 1
        if x[child-1] <= parent_value:
            break
        else:
            x[math.floor(child/2)-1] = x[child-1]
            child = 2 * child
    x[math.floor(child/2)-1] = parent_value


if __name__ == "__main__":
    x = [5, 4, 3, 1, 2, 8, 7, 6, -1];
    print("\n\n\nUnsortierte Liste: ", x)
    heapsort(x)
    print("Liste nach heapsort: ", x)
    x = ['a', 'c', 'd', 'b', 'f', 'e', 'h', 'g', 'y'];
    print("\n\n\nUnsortierte Liste: ", x)
    heapsort(x)
    print("Liste nach heapsort: ", x)
    x = [1, 6, 20, 30, 7, 9, 4, 12, 8]
    print("\n\n\nUnsortierte Liste: ", x)
    heapsort(x)
    print("Liste nach heapsort: ", x)