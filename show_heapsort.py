# Inspired by Kenneth Lange. Algorithms from THE BOOK, Chapter 2. Sorting. SIAM, 2020.


import math
from binarytree import build


def show_heapsort(x):    
    binary_tree = build(x)
    print(binary_tree)
    print("\n\n")

def heapsort(x):
    n = len(x)
    
    print("\n\n\nInitial tree:")
    show_heapsort(x)
    
    print("\n\n\nPhase 1:")
    for parent in range(math.floor(n/2),0,-1): # form the heap
        print("siftdown(x, "+str(parent)+", "+str(n)+")")
        siftdown(x, parent, n)
        print("Tree after siftdown has finished:")
        show_heapsort(x)
        
    print("\n\n\nPhase 2:")
    for bottom in range(n,1,-1): # sort the heap
        print("Swap root element with bottom element:")
        x[0], x[bottom-1] = x[bottom-1], x[0]
        show_heapsort(x)
        print("siftdown(x, 1, "+str(bottom-1)+")")
        siftdown(x, 1, bottom - 1)
        print("Tree after siftdown has finished:")
        show_heapsort(x)


def siftdown(x, parent, bottom):
    parent_value = x[parent-1]
    child = 2 * parent
    while child <= bottom:
        if child < bottom and x[child-1] < x[child]:
            child = child + 1
        if x[child-1] <= parent_value:
            break
        else:
            x[math.floor(child/2)-1] = x[child-1]
            child = 2 * child
    x[math.floor(child/2)-1] = parent_value


if __name__ == "__main__":
    x = [1, 6, 20, 30, 7, 9, 4, 12, 8]
    print("\n\n\nUnsortierte Liste: ", x)
    heapsort(x)
    print("\n\n\nListe nach heapsort: ", x)