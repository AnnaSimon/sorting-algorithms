from queue import PriorityQueue
  
pq = PriorityQueue()                                # empty queue
print("Priority Queue is empty:", pq.empty())
  

pq.put((5, 'e'))                                    # enqueue (insert)
pq.put((2, 'u'))
pq.put((7, 'u'))
pq.put((1, 'Q'))  
pq.put((9, 'e'))


for i in range(5):                                  # dequeue (remove and return lowest priority item)
    print(pq.get())


pq.put((7, 'u'))
pq.put((3, 'Q'))  
pq.put((9, 'e'))  
print("Priority Queue:", pq.queue)


print("Lowest priority item:", pq.queue[0])         # peek


print("Size of the Priority Queue:", pq.qsize())    # return size
  
