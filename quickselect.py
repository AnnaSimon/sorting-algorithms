# Inspired by Kenneth Lange. Algorithms from THE BOOK, Chapter 2. Sorting. SIAM, 2020.

import numpy as np


def quickselect(x, k, left = 0, right = None):  # left <= k <= right
    if right is None:			                # default
        right = len(x)-1
    i = np.random.randint(left, right+1)	    # select a random splitting value, returns a value from left (inclusive) to right+1 (exclusive)
    pivot = x[i]				                # random pivot
    x[left], x[i] = pivot, x[left]		        # swap pivot and element on the left-hand side of the list
    i = left
    for j in range (left+1, right+1):	        # right+1 is not included, only right
        if x[j] <= pivot:
            i += 1
            x[i], x[j] = x[j], x[i]
    x[left], x[i] = x[i], pivot
    
    if k == i:
        y = x[i]
    elif k < i:
        y = quickselect(x, k, left, i-1)
    else:
        y = quickselect(x, k, i+1, right)
    return y
	
	
    
if __name__ == "__main__":	
    k = 6
    x = [5, 4, 3, 1, 2, 8, 7, 6, -1, 3]
    print(x)
    xk = quickselect(x, k-1)
    print("The "+str(k)+"-smallest element is: ", xk)
    k = 5
    x = ['a', 'c', 'd', 'b', 'f', 'e', 'h', 'g']
    print("\n\n")
    print(x)
    xk = quickselect(x, k-1)
    print("The "+str(k)+"-smallest element is: ", xk)	
