# Inspired by Kenneth Lange. Algorithms from THE BOOK, Chapter 2. Sorting. SIAM, 2020.


def f(x):
    return x**3-5*x+1
    
def g(x):
    return 1
    


def bisection(f, a, b, k, epsilon=1e-14):
    fa, fb = f(a), f(b)
    if(a >= b or fa * fb >= 0):                         # check for input error
        return "input error", "no iterations possible"
    for iteration in range(1,k+1):                      # k+1 is excluded
        m = (a + b) / 2
        fm = f(m)
        if abs(fm) < epsilon:
            return m, iteration
        if fa * fm < 0:
            b, fb = m, fm
        else:
            a, fa = m, fm
    return (a+b)/2, k




if __name__ == "__main__":
    x, iteration = bisection(f, 0.0, 2.0, 100)
    print("\n\nbisection(x**3-5*x+1, 0.0, 2.0, 100): ", (x, iteration))

    x, iteration = bisection(f, 0.0, 2.0, 30)
    print("bisection(x**3-5*x+1, 0.0, 2.0, 30): ", (x, iteration))

    x, iteration = bisection(g, 0.0, 2.0, 100)
    print("bisection(0, 0.0, 2.0, 100): ", (x, iteration))

