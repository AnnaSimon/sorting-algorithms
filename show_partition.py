# Inspired by Kenneth Lange. Algorithms from THE BOOK, Chapter 2. Sorting. SIAM, 2020.


import numpy as np
import matplotlib.pyplot as plt


def plot_partition(a, b, pos_pivot, my_i, left, right, title = "", print_ij = False, i = -1, j = -1):

    length_b = len(b)
    my_b = []
    for l in range(length_b):
        my_b.append("x["+str(l)+"]")

    fig, ax = plt.subplots()
    bars = ax.bar(a, b, width=0.9, linewidth=0.7)
    bars[pos_pivot].set_color("red")
    ax.set(xlim = (0, length_b), xticks = np.arange(length_b)+0.5, ylim = (-3, 10), yticks = np.arange(-2, 10))
    ax.set_xticklabels(my_b, minor = False)
    plt.suptitle("partition(x,"+str(left)+","+str(right)+","+str(my_i)+")", fontweight = "bold")
    plt.title(title) 
    patches = ax.patches
    labels = ["" for k in range(length_b)]
    labels[pos_pivot] = "pivot"
    
    if print_ij == True:
        if i==j:
            labels[j] = "j, i"
        else:
            labels[j] = "j"
            if i == pos_pivot:
                labels[i] = "pivot, i"
            else:
                labels[i] = "i"
                
    for patch, label in zip(patches, labels):
        x_value = patch.get_x() + patch.get_width() / 2
        y_value = patch.get_height() 
        position = "bottom"
        if y_value < 0:
            position = "top"
        ax.text(x_value, y_value, label, ha = "center", va = position)
    plt.show()
    
    
    
    
def partition(x, left, right, i):
    pivot = x[i]				
    my_i = i
    a = 0.5 + np.arange(len(x))
    plot_partition(a, x, i, my_i, left, right)
    x[left], x[my_i] = pivot, x[left]		
    i=left
    plot_partition(a, x, i, my_i, left, right, "swap pivot and element on the left-hand side")
    for j in range (left+1, right+1):	
        plot_partition(a, x, left, my_i, left, right, "iteration: j ="+str(j), True, i, j)
        if x[j] <= pivot:
            i += 1
            plot_partition(a, x, left, my_i, left, right, "x["+str(j)+"]="+str(x[j])+"<= pivot    ---------> i+=1", True, i, j)
            x[i], x[j] = x[j], x[i]
            plot_partition(a, x, left, my_i, left, right, "swap x["+str(j)+"] and x["+str(i)+"]", True, i, j)
    x[left], x[i] = x[i], pivot
    plot_partition(a, x, i, my_i, left, right, "swap pivot and x["+str(i)+"]")
    return i





def quicksort(x, left=0, right = None):
    if right is None:			                # default
        right = len(x)-1
    split = np.random.randint(left, right+1)	# select a random splitting value
    i = partition(x,left,right,split)
    if i>left+1:
        quicksort(x, left, i-1)
    if i+1<right:
        quicksort(x, i+1, right)
	
	
	
if __name__ == "__main__":	
    x = [5, 4, 3, 1, 2, 8, 7, 6, -1]
    #x = [5, 4, 3, 8, -1]
    #quicksort(x)
    print("before partition: ", x)
    partition(x, 0, len(x)-1, 1)
    print("after partition around value 4: ", x)
    #y = [2, 8, 3, 9, 4, 5, 1, -2]
    #partition(y, 1, len(y)-2, 4)
