# Inspired by Kenneth Lange. Algorithms from THE BOOK, Chapter 2. Sorting. SIAM, 2020.


import math
from binarytree import build


def show_siftdown(x):
    binary_tree = build(x)
    print(binary_tree)



def siftdown(x, parent, bottom):
    print("\n\n\n\nSiftdown(x, "+str(parent)+", "+str(bottom)+")")
    parent_value = x[parent-1]
    print("parent-value:", parent_value)
    print("Initial tree:")
    show_siftdown(x)
    child = 2 * parent
    while child <= bottom:
        if child < bottom and x[child-1] < x[child]:
            child = child + 1
        print("max. child-value:", x[child-1])
        if x[child-1] <= parent_value:
            break
        else:
            x[math.floor(child/2)-1] = x[child-1]
            child = 2 * child
            print("\"Switch\" max. child-value with parent-value:")
            y = x
            y[math.floor(child/2)-1] = str(parent_value)+" ("+str(y[math.floor(child/2)-1])+")"
            show_siftdown(x)
    x[math.floor(child/2)-1] = parent_value
    print("Tree after siftdown has finished:")
    show_siftdown(x)


if __name__ == "__main__":	
    #x = [1, 6, 20, 30, 7, 9, 4, 12, 8]
    #print(x)
    #siftdown(x, 4, len(x))
    #siftdown(x, 3, len(x))
    x = [40, 6, 20, 30, 7, 9, 4, 12, 8]
    print(x)
    siftdown(x, 2, len(x))
    #siftdown(x, 1, len(x))
