# Inspired by Kenneth Lange. Algorithms from THE BOOK, Chapter 2. Sorting. SIAM, 2020.


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches


def plot_quickselect(a, b, k, pos_pivot, already_excluded, left, right, title = ""):

    length_b = len(b)
    fig, ax = plt.subplots()
    bars = ax.bar(a, b, width = 0.9, linewidth = 0.7, color = "blue")
    
    my_b = []
    for l in range(length_b):
        my_b.append("x["+str(l)+"]")
        
    blue = mpatches.Patch(color = "blue", label = "not yet excluded")
    red = mpatches.Patch(color = "red", label = "pivot")
    grey = mpatches.Patch(color = "grey", label = "already excluded")
    plt.legend(handles = [blue, red, grey], fontsize = "x-small")
    
    bars[pos_pivot].set_color("red")
    for el in already_excluded:
        bars[el].set_color("grey")
    
    ax.set(xlim = (0, length_b), xticks = np.arange(length_b)+0.5, ylim = (-3, 11), yticks = np.arange(-2, 11))
    ax.set_xticklabels(my_b, minor = False)
    plt.suptitle("quickselect(x,"+str(k)+","+str(left)+","+str(right)+")", fontweight = "bold")
    plt.title(title)
    patches = ax.patches
    labels = ["" for k in range(length_b)]
    labels[pos_pivot] = "pivot"
    for patch, label in zip(patches, labels):
        x_value = patch.get_x() + patch.get_width() / 2
        y_value = patch.get_height()
        position = "bottom"
        if y_value < 0:
            position = "top"
        ax.text(x_value, y_value, label, ha = "center", va = position)
    plt.show()
   




def quickselect(x, k, left = 0, right = None, already_excluded = []): # left<=k<=right

    if right is None:			# default
        right = len(x)-1
        
    i = np.random.randint(left, right+1)	# select a random splitting value, returns a value from left (inclusive) to right+1 (exclusive)
    pivot = x[i]				# random pivot
    a = 0.5 + np.arange(len(x))
    plot_quickselect(a, x, k, i, already_excluded, left, right, "random pivot")
    x[left], x[i] = pivot, x[left]		# swap pivot and element on the left-hand side of the list
    i = left
    plot_quickselect(a, x, k, i, already_excluded, left, right, "swap pivot and element on the left-hand side")

    for j in range (left+1, right+1):	# right+1 is not included, only right
        if x[j] <= pivot:
            i += 1
            x[i], x[j] = x[j], x[i]
    x[left], x[i] = x[i], pivot

    plot_quickselect(a, x, k, i, already_excluded, left, right, "pivot sorted correctly")

    if k == i:
        plot_quickselect(a, x, k, i, already_excluded, left, right, "k = "+str(k)+" = i: we found the k-th smallest element: "+str(pivot))
        y = x[i]
    elif k < i:
        already_excluded.extend(range(i, right+1))
        plot_quickselect(a, x, k, i, already_excluded, left, right, "k = "+str(k)+" < "+str(i)+" = i: exclude elements with positions from i = "+str(i)+" to right = "+str(right))
        y = quickselect(x, k, left, i-1)
    else:
        already_excluded.extend(range(left, i+1))
        plot_quickselect(a, x, k, i, already_excluded, left, right, "k = "+str(k)+" < "+str(i)+" = i: exclude elements with positions from left = "+str(left)+" to i = "+str(i))
        y = quickselect(x, k, i+1, right)
    return y
	
	

	
if __name__ == "__main__":	
    #k = 6
    #x = [5, 4, 3, 1, 2, 8, 7, 6, -1, 3]
    k = 3
    x = [5, 2, 1, 8, -1]
    print(x)
    xk = quickselect(x, k-1)
    print("The "+str(k)+"-smallest element is: ", xk)
    
    
    
    
    
    
    
 
	
